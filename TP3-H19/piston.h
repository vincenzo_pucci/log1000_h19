#ifndef __PISTON__
#define __PISTON__

class Piston {
	private:
		int volume;
	public:
		Piston(int volume);
		int getVolume();
};

#endif

